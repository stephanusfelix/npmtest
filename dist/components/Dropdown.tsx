import React, { HTMLAttributes } from 'react';
import './styles/Dropdown.scss'

type DropdownProps ={
    onChange?: () => void;
} & HTMLAttributes<HTMLSelectElement> 

export const MaiuiDropdown = ({
    ...props
}: DropdownProps) => {
    return (
        <form className='maiui-dropdown' onChange={props.onChange}>
            <select className='maiui-dropdown-select'>
                {props?.children}
            </select>
        </form>
    );
};