import React from 'react';
import './styles/Switch.scss'


type SwitchProps = {
    onChange?: () => void;
  }
  
  export const MaiuiSwitch = ({
    ...props
  }: SwitchProps) => {
    return (
      <label className='maiui-switch'>
          <input className='maiui-switch-input' type='checkbox' onChange={props.onChange}/>
          <span className='maiui-switch-slider maiui-switch-round'></span>
      </label>
    );
  };