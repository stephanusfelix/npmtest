import React, { HTMLAttributes} from 'react';
import './styles/Card.scss'

type CardProps   = {
    msize: 'small' | 'medium' | 'large',

    className?: string
} & HTMLAttributes<HTMLElement>
export const MaiuiCard = ({
  msize = 'medium',
  ...props
}: CardProps) => {
  return (
    <div
      className={['maiui-card', `maiui-card--${msize}`].join(' ')}
      {...props}
    >
        {props.children}
    </div>
  );
};
