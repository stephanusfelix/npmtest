import React, { ForwardRefRenderFunction, InputHTMLAttributes } from 'react';
import './styles/Input.scss'


type MaiuiInputProps = {
    name?: string;
    msize?: 'medium' | 'large';
    placeholder?:string;
    type?: string
} &  InputHTMLAttributes<HTMLInputElement>

export const MaiuiInput = ({
    msize = 'medium',
    placeholder,
    type,
    ...props
}: MaiuiInputProps) => {
    return (
        <input
            className={['maiui-input', `maiui-input--${msize}`].join(' ')}
            type={type}
            placeholder={placeholder}
            {...props}
        >
        </input>
    );
};
  