import React, { HTMLAttributes } from 'react';
import './styles/DropdownItem.scss'

type DropdownItemProps = {
    selected?: boolean;
    onChange?: () => void;
    value?: string;
} & HTMLAttributes<HTMLOptionElement>

/**
 * Primary UI component for user interaction
 */
export const MaiuiDropdownItem = ({
    value,
    selected,
    ...props
}: DropdownItemProps) => {
    return (
        <option className='maiui-dropdownitem' selected={selected} {...props}>
            {value}
        </option>
    );
};
