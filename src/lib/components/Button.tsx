import React from 'react';
import './styles/Button.scss'


type ButtonProps ={
    primary?: boolean;
    backgroundColor?: string;
    size?: 'small' | 'medium' | 'large';
    label: string;
    onClick?: () => void;
  }
  export const MaiuiButton = ({
    primary = false,
    size = 'medium',
    backgroundColor,
    label,
    ...props
  }: ButtonProps) => {
    const mode = primary ? 'maiui-button--primary' : 'maiui-button--secondary';
    return (
      <button
        type="button"
        className={['maiui-button', `maiui-button--${size}`, mode].join(' ')}
        style={{ backgroundColor }}
        {...props}
      >
        {label}
      </button>
    );
  };
  